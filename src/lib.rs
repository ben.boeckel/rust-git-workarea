// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![deny(missing_docs)]
// XXX(rust-1.66)
#![allow(clippy::uninlined_format_args)]

//! Git workarea
//!
//! This crate implements a workarea given a bare git repository. A workarea is like a worktree
//! except that its on-disk representation is minimal so that operations are not constrained by
//! disk speed.

mod git;
mod prepare;

pub use git::CommitId;
pub use git::GitContext;
pub use git::GitError;
pub use git::Identity;
pub use git::MergeStatus;

pub use prepare::Conflict;
pub use prepare::GitWorkArea;
pub use prepare::MergeCommand;
pub use prepare::MergeResult;
pub use prepare::SubmoduleConfig;
pub use prepare::WorkAreaError;

#[cfg(test)]
mod test;
